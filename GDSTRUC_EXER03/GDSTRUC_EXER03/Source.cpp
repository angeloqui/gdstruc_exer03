#include <iostream>
#include <string>

using namespace std;

int main() {

	//1. Declare a name for the guild (user input)
	string name;
	
	cout << "Declare a name for the guild: ";
	cin >> name;

	//2. Declare an initial size for the guild(user input)
	int size;

	cout << "Declare an initial size for the guild: ";
	cin >> size;

	//3. Input the usernames of the initial members (user input)
	string* guild = new string[size];

	cout << "Input the usernames of the initial members" << endl;
	for (int i = 0; i < size; i++) {
		cout << i << ". ";
		cin >> guild[i];
	}

	system("pause");
	system("cls");

	while (true) {
		//4. Operations:
		int option;

		cout << "Operations:"
			<< "\n1 - Print every member"
			<< "\n2 - Renaming a member"
			<< "\n3 - Adding a member"
			<< "\n4 - Deleting a member[EMPTY SLOT]" << endl;

		cout << "\nOption: ";
		cin >> option;

		if (option == 1) {
			// - Print every member
			for (int i = 0; i < size; i++) {
				cout << i << ". " << guild[i] << endl;
			}
		}
		else if (option == 2) {
			// - Renaming a member
			int n;
			string s;
			
			cout << "Enter guild index: ";
			cin >> n;

			cout << "Enter new name: ";
			cin >> s;

			guild[n] = s;
		}
		else if (option == 3) {
			// - Adding a member
			string s;

			size++;
			string* temp = new string[size];

			for (int i = 0; i < size - 1; i++) {
				temp[i] = guild[i];
			}

			cout << "Enter new name: ";
			cin >> s;

			temp[size - 1] = s;

			delete[] guild;
			guild = new string[size];

			for (int i = 0; i < size; i++) {
				guild[i] = temp[i];
			}

			delete[] temp;

		}
		else if (option == 4) {
			// - Deleting a member [EMPTY SLOT]
			int n;
			string str;

			cout << "Enter guild index: ";
			cin >> n;
			str = guild[n];

			for (int i = n; i < size - 1; i++) {
				guild[i] = guild[i + 1];
			}

			size--;
			string* temp = new string[size];

			for (int i = 0; i < size; i++) {
				temp[i] = guild[i];
			}

			delete[] guild;
			guild = new string[size];

			for (int i = 0; i < size; i++) {
				guild[i] = temp[i];
			}

			delete[] temp;



		}

		system("pause");
		system("cls");
	}
}